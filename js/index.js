$(function(){
    $("[data-toggle='popover']").popover();
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval: 1500
    });   

    $('#contacto').on('show.bs.modal', function (e) {
        console.log("El modal de contacto se está mostrando");
        $('#contacto-btn').removeClass('btn-outline-success');
        $('#contacto-btn').addClass('btn-primary');
        $('#contacto-btn').prop('disabled', true);
    });
        console.log("El modal de contacto se mostró");
        $('#contacto').on('hide.bs.modal', function (e) {
        console.log("El modal de contacto se está ocultando");
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log("El modal de contacto se ocultó");
        $('#contacto-btn').prop('disabled', false);
        $('#contacto-btn').removeClass('btn-primary');
        $('#contacto-btn').addClass('btn-outline-success');
        $('#contacto-btn').prop('disabled', false);
    });            
}); 
